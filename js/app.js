jQuery(document).ready(function($){
    var html = jQuery('.site-wrapper').html();    
    jQuery('#load-more').click(function(e){
        e.preventDefault();
        var dis = jQuery(this);
        dis.hide(0);
        jQuery('.spinner').removeClass('loading-done');
        
        setTimeout(function(){
            jQuery('.site-wrapper').append(html);
            dis.show(0);
            jQuery('.spinner').addClass('loading-done');
        },2000);
    });    
});